var express = require('express');
var path = require('path');
var cors = require('cors');
var mysql = require('mysql');
var app = express();


app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  })
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));



var mysqlConnection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root"
});

var databaseConnection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "users"
});

var usersDatabaseData = [];

app.post('/getUsers', function(req, res) {
  mysqlConnection.connect(function(err, result) {
    if (err) { 
      throw err;
    }
    else {
      console.log("MySQL Connected!", result);
    }
  })
  // mysqlConnection.query("CREATE DATABASE if not exists users", function (err, result)     {
  //   if (err) {
  //       throw err;
  //   }
  //   else {
  //       console.log("Database Created", result);
  //   }
  // })
  var select = "SELECT * FROM users"
      databaseConnection.query(select, function(err, result) {
        if(err) {
          return console.error(err.message);
        }
        else {
          console.log("Retrieved from users table");
          usersDatabaseData = result;
          console.log(result);
          res.end(JSON.stringify({ userDetails: result }));
          
        }
      });      
  })

app.post('/users', function(req, res) {
        console.log("Decoded", req.body);
        

        // mysqlConnection.connect(function(err, result) {
        //   if (err) { 
        //     throw err;
        //   }
        //   else {
        //     console.log("MySQL Connected!", result);
        //   }
        // })
        mysqlConnection.query("CREATE DATABASE if not exists users", function (err, result) {
              if (err) {
                  throw err;
              }
              else {
                  console.log("Database Created", result);
              }
        })

        var create = "CREATE TABLE if not exists users(user_id int primary key auto_increment, last_name varchar(255)not null, first_name varchar(255), birth_date date, mobile_number varchar(20), email_address varchar(255)  )";
        databaseConnection.query(create, function (err, result) {
          if (err) { 
              throw err;
          }
          else {
            console.log("Table Created", result) ; 
          }
        });
        
        var insert = "INSERT INTO users(user_id, last_name, first_name, birth_date, mobile_number, email_address) VALUES(?,?,?,?,?,?)";
        var data = [ req.body.user_id, req.body.last_name, req.body.first_name, req.body.birth_date, req.body.mobile_number, req.body.email_address ];
        databaseConnection.query(insert, data, function(err, result) {
          if(err) {
            return console.error(err.message);
          }
          else {
              console.log("Record Inserted!!");
              console.log(result);
          }
        });

        var select = "SELECT * FROM users"
        databaseConnection.query(select, function(err, result) {
          if(err) {
            return console.error(err.message);
          }
          else {
            console.log("Retrieved Data from users table");
            usersDatabaseData = result;
            console.log(result);
            res.end(JSON.stringify({"userDetails": result }));
            
          }
        }); 
});

app.post('/updateUsers', function(req, res) {
  console.log("Update Users", req.body);
  var update = "UPDATE users SET last_name = ?, first_name = ?, birth_date = ?, mobile_number = ?, email_address = ? WHERE user_id = ?";
  var data = [ req.body.last_name, req.body.first_name, req.body.birth_date, req.body.mobile_number, req.body.email_address, req.body.user_id ]
      databaseConnection.query(update, data, function(err, result) {
        if(err) {
          return console.error(err.message);
        }
        else {
            console.log("User Detail Updated!!");
            console.log(result);
        }
      });

      var select = "SELECT * FROM users"
      databaseConnection.query(select, function(err, result) {
        if(err) {
          return console.error(err.message);
        }
        else {
          console.log("Retrieved Data from users table");
          usersDatabaseData = result;
          console.log(result);
          res.end(JSON.stringify({"userDetails": result }));            
        }
      });    
});

app.post('/deleteUsers', function(req, res) {
  console.log("Delete Users", req.body);
  var deleteQuery = "DELETE FROM users WHERE user_id = ?";
    databaseConnection.query(deleteQuery, req.body.user_id, function(err, result) {
      if(err) {
        return console.error(err.message);
      }
      else {
          console.log("Selected User Deleted!!");
          console.log(result);
      }
    });

    var select = "SELECT * FROM users"
      databaseConnection.query(select, function(err, result) {
        if(err) {
          return console.error(err.message);
        }
        else {
          console.log("Retrieved Data from users table");
          usersDatabaseData = result;
          console.log(result);
          res.end(JSON.stringify({"userDetails": result }));            
        }
      });    
});



app.post('/testAPI', function(req, res) {
  console.log('Inside Home Login', req);
  res.end(JSON.stringify({ "name": "jebakumar" }));
});

app.listen(9000, () => {
  console.log("Express started")
})
